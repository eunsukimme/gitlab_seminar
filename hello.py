class Hello:
    def greeting(self):
        return 'hello world'

    def add(self, a, b):
        return a + b


if __name__ == '__main__':
    hello = Hello()
    print(hello.add(1, 2))
    print(hello.greeting())
